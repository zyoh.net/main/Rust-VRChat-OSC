use std::net::UdpSocket;

use vrchat_osc::io;

fn main() {
    // Default VRChat sending address (VRChat -> Application).
    let vrchat_sending_address = "127.0.0.1:9001";
    let socket = UdpSocket::bind(vrchat_sending_address).unwrap();

    let result = print_messages(socket);
    println!("{:?}", result);
}

fn print_messages(socket: UdpSocket) -> Result<(), Box<dyn std::error::Error>> {
    let mut buf = [0u8; rosc::decoder::MTU];

    loop {
        let (size, _) = socket.recv_from(&mut buf)?;
        let (_, packet) = rosc::decoder::decode_udp(&buf[..size])?;
        if let rosc::OscPacket::Message(msg) = packet {

            // Get output from message.
            let output: io::Output = io::Output::try_from(msg)?;

            // Look for gestures and print them.
            match output {
                io::Output::GestureLeft(gesture) | io::Output::GestureRight(gesture) => {
                    println!("Gesture: {:?}", gesture);
                }
                _ => {}
            }

        }
    }
}

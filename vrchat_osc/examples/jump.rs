use vrchat_osc::io;

fn main() {
    // Default VRChat listening address (Application -> VRChat).
    let vrchat_input_address: &str = "127.0.0.1:9000";
    // 127.0.0.1 port 0 is a random port on localhost
    let socket = std::net::UdpSocket::bind("127.0.0.1:0").unwrap();

    let messages = &[
        io::Input::Jump(true),
        io::Input::Jump(false),
    ];

    for message in messages {
        let result = send(message.clone(), vrchat_input_address, &socket);
        println!("{:?}", result);
        // Wait 0.1 seconds before sending the next message.
        std::thread::sleep(std::time::Duration::from_millis(100));
    }
}

fn send(message: vrchat_osc::io::Input, address: &str, socket: &std::net::UdpSocket) -> Result<usize, Box<dyn std::error::Error>> {
    let packet: rosc::OscPacket = rosc::OscPacket::Message(message.into());
    let bytes: Vec<u8> = rosc::encoder::encode(&packet)?;

    Ok(socket.send_to(&bytes, address)?)
}

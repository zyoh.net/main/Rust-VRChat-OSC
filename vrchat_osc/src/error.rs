use std::{error::Error, fmt::{Display, Formatter, self}};

use crate::io::ParameterType;

/// Used when address and value type are valid, but value is invalid (e.g. out of range).
#[derive(Debug)]
pub struct InvalidAvatarValue {
    pub location: &'static str,
    pub value: ParameterType
}

impl Error for InvalidAvatarValue {}

impl Display for InvalidAvatarValue {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let display = "Invalid value for ".to_owned() + self.location + ": " + &format!("{:?}", self.value);
        write!(f, "{}", display)
    }
}

/// Used when address or value type are invalid.
#[derive(Debug)]
pub struct InvalidOscMessage {
    pub location: &'static str,
    pub value: rosc::OscMessage
}

impl Error for InvalidOscMessage {}

impl Display for InvalidOscMessage {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let display = "Invalid OscMessage for ".to_owned() + self.location + ": " + &format!("{:?}", self.value);
        write!(f, "{}", display)
    }
}

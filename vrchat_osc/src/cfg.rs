use std::{
    any::type_name,
    str::FromStr,
};

use json::JsonValue;

#[derive(Debug, Clone)]
pub struct AvatarConfig {
    pub id: String,
    pub name: String,
    pub parameters: Vec<ConfigParameter>
}

impl TryFrom<JsonValue> for AvatarConfig {
    type Error = String;

    fn try_from(value: JsonValue) -> Result<Self, Self::Error> {
        if let (
            Some(id),
            Some(name)
        ) = (
            value["id"].as_str(),
            value["name"].as_str(),
        ) {
            let mut parameters = Vec::new();

            if let JsonValue::Array(p) = &value["parameters"] {
                parameters.extend_from_slice(
                    &p
                    .iter()
                    .flat_map(|x| x.try_into())
                    .collect::<Vec<ConfigParameter>>()
                );
            }

            return Ok(
                Self {
                    id: id.to_string(),
                    name: name.to_string(),
                    parameters
                }
            );
        }

        Err(format!("Invalid JSON for {}", type_name::<Self>()))
    }
}

#[derive(Debug, Clone)]
pub struct ConfigParameter {
    pub name: String,
    pub input: Option<ConfigAddress>,
    pub output: Option<ConfigAddress>,
}

impl TryFrom<&JsonValue> for ConfigParameter {
    type Error = String;

    fn try_from(value: &JsonValue) -> Result<Self, Self::Error> {
        if let Some(name) = value["name"].as_str() {
            let input = (&value["input"]).try_into().ok();
            let output = (&value["output"]).try_into().ok();

            return Ok(
                Self {
                    name: name.to_string(),
                    input,
                    output,
                }
            );
        }

        Err(format!("Invalid JSON for {}", type_name::<Self>()))
    }
}

#[derive(Debug, Clone)]
pub struct ConfigAddress {
    pub address: String,
    pub cfgtype: ConfigType,
}

impl TryFrom<&JsonValue> for ConfigAddress {
    type Error = String;

    fn try_from(value: &JsonValue) -> Result<Self, Self::Error> {
        if let (
            Some(address),
            Some(ty),
        ) = (
            value["address"].as_str(),
            value["type"].as_str()
        ) {
            return Ok(
                Self {
                    address: address.to_string(),
                    cfgtype: ConfigType::from_str(ty)?,
                }
            );
        }

        Err(format!("Invalid JSON for {}", type_name::<Self>()))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ConfigType {
    Int,
    Float,
    Bool
}

impl FromStr for ConfigType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "int" => Ok(ConfigType::Int),
            "float" => Ok(ConfigType::Float),
            "bool" => Ok(ConfigType::Bool),
            _ => Err("Invalid address type".into())
        }
    }
}

#[test]
fn test_avatar_config() {
    let avatar_json =
        json::parse(include_str!("../tests/avatar.json"))
            .expect("Test config is not valid JSON");

    let avatar_config =
        AvatarConfig::try_from(avatar_json)
            .expect("Test config does not map to struct");

    assert_eq!(
        avatar_config.id,
        "avtr_10101010-1010-1010-1010-101010101010".to_string()
    );

    assert_eq!(
        avatar_config.name,
        "Name here".to_string()
    );

    assert_eq!(
        avatar_config.parameters[1].name,
        "InStation".to_string()
    );

    assert!(avatar_config.parameters[1].input.is_none());

    let cfgaddr = avatar_config.parameters[1].output.clone().unwrap();
    assert_eq!(
        cfgaddr.address,
        "/avatar/parameters/InStation".to_string()
    );

    assert_eq!(
        cfgaddr.cfgtype,
        ConfigType::Bool
    );
}

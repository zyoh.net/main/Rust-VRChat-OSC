use crate::error;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ParameterType {
    Bool(bool),
    Float(f32),
    Int(u8),
}

impl From<ParameterType> for rosc::OscType {
    fn from(val: ParameterType) -> Self {
        match val {
            ParameterType::Bool(value) => rosc::OscType::Bool(value),
            ParameterType::Float(value) => rosc::OscType::Float(value),
            ParameterType::Int(value) => rosc::OscType::Int(value as i32),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Gesture {
    None,
    Fist,
    OpenHand,
    FingerPoint,
    Victory,
    RockNRoll,
    HandGun,
    ThumbsUp,
}

impl From<Gesture> for i32 {
    fn from(val: Gesture) -> Self {
        match val {
            Gesture::None => 0,
            Gesture::Fist => 1,
            Gesture::OpenHand => 2,
            Gesture::FingerPoint => 3,
            Gesture::Victory => 4,
            Gesture::RockNRoll => 5,
            Gesture::HandGun => 6,
            Gesture::ThumbsUp => 7,
        }
    }
}

impl TryFrom<i32> for Gesture {
    type Error = error::InvalidAvatarValue;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Gesture::None),
            1 => Ok(Gesture::Fist),
            2 => Ok(Gesture::OpenHand),
            3 => Ok(Gesture::FingerPoint),
            4 => Ok(Gesture::Victory),
            5 => Ok(Gesture::RockNRoll),
            6 => Ok(Gesture::HandGun),
            7 => Ok(Gesture::ThumbsUp),
            _ => Err(
                error::InvalidAvatarValue {
                    location: stringify!(Gesture),
                    value: ParameterType::Int(value as u8),
                }
            )
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Viseme {
    Sil,
    Pp,
    Ff,
    Th,
    Dd,
    Kk,
    Ch,
    Ss,
    Nn,
    Rr,
    Aa,
    E,
    I,
    O,
    U,
}

impl From<Viseme> for i32 {
    fn from(val: Viseme) -> Self {
        match val {
            Viseme::Sil => 0,
            Viseme::Pp => 1,
            Viseme::Ff => 2,
            Viseme::Th => 3,
            Viseme::Dd => 4,
            Viseme::Kk => 5,
            Viseme::Ch => 6,
            Viseme::Ss => 7,
            Viseme::Nn => 8,
            Viseme::Rr => 9,
            Viseme::Aa => 10,
            Viseme::E => 11,
            Viseme::I => 12,
            Viseme::O => 13,
            Viseme::U => 14,
        }
    }
}

impl TryFrom<i32> for Viseme {
    type Error = error::InvalidAvatarValue;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Viseme::Sil),
            1 => Ok(Viseme::Pp),
            2 => Ok(Viseme::Ff),
            3 => Ok(Viseme::Th),
            4 => Ok(Viseme::Dd),
            5 => Ok(Viseme::Kk),
            6 => Ok(Viseme::Ch),
            7 => Ok(Viseme::Ss),
            8 => Ok(Viseme::Nn),
            9 => Ok(Viseme::Rr),
            10 => Ok(Viseme::Aa),
            11 => Ok(Viseme::E),
            12 => Ok(Viseme::I),
            13 => Ok(Viseme::O),
            14 => Ok(Viseme::U),
            _ => Err(
                error::InvalidAvatarValue {
                    location: stringify!(Viseme),
                    value: ParameterType::Int(value as u8),
                }
            ),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TrackingType {
    Uninitialized,
    Generic,
    Av2ThreePoint,
    ThreePoint,
    FourPoint,
    SixPoint,
}

impl From<TrackingType> for i32 {
    fn from(val: TrackingType) -> Self {
        match val {
            TrackingType::Uninitialized => 0,
            TrackingType::Generic => 1,
            TrackingType::Av2ThreePoint => 2,
            TrackingType::ThreePoint => 3,
            TrackingType::FourPoint => 4,
            TrackingType::SixPoint => 6,
        }
    }
}

impl TryFrom<i32> for TrackingType {
    type Error = error::InvalidAvatarValue;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(TrackingType::Uninitialized),
            1 => Ok(TrackingType::Generic),
            2 => Ok(TrackingType::Av2ThreePoint),
            3 => Ok(TrackingType::ThreePoint),
            4 => Ok(TrackingType::FourPoint),
            6 => Ok(TrackingType::SixPoint),
            _ => Err(
                error::InvalidAvatarValue {
                    location: stringify!(TrackingType),
                    value: ParameterType::Int(value as u8),
                }
            ),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ChatboxInput {
    pub text: String,
    pub immediate: bool,
    pub notify: bool,
}

impl From<ChatboxInput> for Vec<rosc::OscType> {
    fn from(val: ChatboxInput) -> Self {
        vec![
            rosc::OscType::String(val.text),
            rosc::OscType::Bool(val.immediate),
            rosc::OscType::Bool(val.notify),
        ]
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum TrackerName {
    Head,
    Other(u8),
}

impl From<TrackerName> for String {
    fn from(val: TrackerName) -> Self {
        match val {
            TrackerName::Head => "head".to_string(),
            TrackerName::Other(num) => format!("{}", num),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct TrackerPosition {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl From<TrackerPosition> for Vec<rosc::OscType> {
    fn from(val: TrackerPosition) -> Self {
        vec![
            rosc::OscType::Float(val.x),
            rosc::OscType::Float(val.y),
            rosc::OscType::Float(val.z),
        ]
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct TrackerRotation {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl From<TrackerRotation> for Vec<rosc::OscType> {
    fn from(val: TrackerRotation) -> Self {
        vec![
            rosc::OscType::Float(val.x),
            rosc::OscType::Float(val.y),
            rosc::OscType::Float(val.z),
        ]
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vector2 {
    pub x: f32,
    pub y: f32,
}

impl From<Vector2> for Vec<rosc::OscType> {
    fn from(val: Vector2) -> Self {
        vec![
            rosc::OscType::Float(val.x),
            rosc::OscType::Float(val.y),
        ]
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl From<Vector3> for Vec<rosc::OscType> {
    fn from(val: Vector3) -> Self {
        vec![
            rosc::OscType::Float(val.x),
            rosc::OscType::Float(val.y),
            rosc::OscType::Float(val.z),
        ]
    }
}

/// Messages sent to the game client.
#[derive(Debug, Clone, PartialEq)]
pub enum Input {
    // https://docs.vrchat.com/docs/osc-as-input-controller
    MoveForward(bool),
    MoveBackward(bool),
    MoveLeft(bool),
    MoveRight(bool),
    LookLeft(bool),
    LookRight(bool),
    Jump(bool),
    Run(bool),
    ComfortLeft(bool),
    ComfortRight(bool),
    DropRight(bool),
    UseRight(bool),
    GrabRight(bool),
    DropLeft(bool),
    UseLeft(bool),
    GrabLeft(bool),
    PanicButton(bool),
    QuickMenuToggleLeft(bool),
    QuickMenuToggleRight(bool),
    Voice(bool),

    Vertical(f32),
    Horizontal(f32),
    LookHorizontal(f32),
    UseAxisRight(f32),
    GrabAxisRight(f32),
    MoveHoldFB(f32),
    SpinHoldCwCcw(f32),
    SpinHoldUD(f32),
    SpinHoldLR(f32),

    ChatboxInput(ChatboxInput),
    ChatboxTyping(bool),

    // https://docs.vrchat.com/docs/osc-avatar-parameters
    Avatar(String, ParameterType),

    // https://docs.vrchat.com/docs/osc-trackers
    TrackerPosition(TrackerName, TrackerPosition),
    TrackerRotation(TrackerName, TrackerRotation),

    // https://docs.vrchat.com/docs/osc-eye-tracking
    EyesClosedAmount(f32),
    CenterPitchYaw(Vector2),
    CenterPitchYawDist(Vector3),
    CenterVec(Vector3),
    CenterVecFull(Vector3),
    LeftRightPitchYaw(Vector2, Vector2),
    LeftRightVec(Vector3, Vector3),
}

impl From<Input> for rosc::OscMessage {
    fn from(val: Input) -> Self {
        match val {
            Input::MoveForward(value) => rosc::OscMessage {
                addr: "/input/MoveForward".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::MoveBackward(value) => rosc::OscMessage {
                addr: "/input/MoveBackward".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::MoveLeft(value) => rosc::OscMessage {
                addr: "/input/MoveLeft".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::MoveRight(value) => rosc::OscMessage {
                addr: "/input/MoveRight".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::LookLeft(value) => rosc::OscMessage {
                addr: "/input/LookLeft".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::LookRight(value) => rosc::OscMessage {
                addr: "/input/LookRight".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::Jump(value) => rosc::OscMessage {
                addr: "/input/Jump".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::Run(value) => rosc::OscMessage {
                addr: "/input/Run".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::ComfortLeft(value) => rosc::OscMessage {
                addr: "/input/ComfortLeft".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::ComfortRight(value) => rosc::OscMessage {
                addr: "/input/ComfortRight".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::DropRight(value) => rosc::OscMessage {
                addr: "/input/DropRight".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::UseRight(value) => rosc::OscMessage {
                addr: "/input/UseRight".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::GrabRight(value) => rosc::OscMessage {
                addr: "/input/GrabRight".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::DropLeft(value) => rosc::OscMessage {
                addr: "/input/DropLeft".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::UseLeft(value) => rosc::OscMessage {
                addr: "/input/UseLeft".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::GrabLeft(value) => rosc::OscMessage {
                addr: "/input/GrabLeft".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::PanicButton(value) => rosc::OscMessage {
                addr: "/input/PanicButton".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::QuickMenuToggleLeft(value) => rosc::OscMessage {
                addr: "/input/QuickMenuToggleLeft".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::QuickMenuToggleRight(value) => rosc::OscMessage {
                addr: "/input/QuickMenuToggleRight".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::Voice(value) => rosc::OscMessage {
                addr: "/input/Voice".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::Vertical(value) => rosc::OscMessage {
                addr: "/input/Vertical".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::Horizontal(value) => rosc::OscMessage {
                addr: "/input/Horizontal".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::LookHorizontal(value) => rosc::OscMessage {
                addr: "/input/LookHorizontal".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::UseAxisRight(value) => rosc::OscMessage {
                addr: "/input/UseAxisRight".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::GrabAxisRight(value) => rosc::OscMessage {
                addr: "/input/GrabAxisRight".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::MoveHoldFB(value) => rosc::OscMessage {
                addr: "/input/MoveHoldFB".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::SpinHoldCwCcw(value) => rosc::OscMessage {
                addr: "/input/SpinHoldCwCcw".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::SpinHoldUD(value) => rosc::OscMessage {
                addr: "/input/SpinHoldUD".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::SpinHoldLR(value) => rosc::OscMessage {
                addr: "/input/SpinHoldLR".to_owned(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::ChatboxInput(value) => rosc::OscMessage {
                addr: "/chatbox/input".to_owned(),
                args: value.into(),
            },
            Input::ChatboxTyping(value) => rosc::OscMessage {
                addr: "/chatbox/typing".to_owned(),
                args: vec![rosc::OscType::Bool(value)],
            },
            Input::Avatar(addr, avatar_value) => rosc::OscMessage {
                addr: format!("/avatar/parameters/{}", addr.strip_prefix('/').unwrap_or(&addr)),
                args: vec![avatar_value.into()],
            },
            Input::TrackerPosition(name, position) => rosc::OscMessage {
                addr: format!("/tracking/trackers/{}/position", String::from(name)),
                args: position.into(),
            },
            Input::TrackerRotation(name, rotation) => rosc::OscMessage {
                addr: format!("/tracking/trackers/{}/rotation", String::from(name)),
                args: rotation.into(),
            },
            Input::EyesClosedAmount(value) => rosc::OscMessage {
                addr: "/tracking/eye/EyesClosedAmount".to_string(),
                args: vec![rosc::OscType::Float(value)],
            },
            Input::CenterPitchYaw(value) => rosc::OscMessage {
                addr: "/tracking/eye/CenterPitchYaw".to_string(),
                args: value.into(),
            },
            Input::CenterPitchYawDist(value) => rosc::OscMessage {
                addr: "/tracking/eye/CenterPitchYawDist".to_string(),
                args: value.into(),
            },
            Input::CenterVec(value) => rosc::OscMessage {
                addr: "/tracking/eye/CenterVec".to_string(),
                args: value.into(),
            },
            Input::CenterVecFull(value) => rosc::OscMessage {
                addr: "/tracking/eye/CenterVecFull".to_string(),
                args: value.into(),
            },
            Input::LeftRightPitchYaw(left, right) => rosc::OscMessage {
                addr: "/tracking/eye/LeftRightPitchYaw".to_string(),
                args: {
                    let mut left: Vec<rosc::OscType> = left.into();
                    let right: Vec<rosc::OscType> = right.into();
                    left.extend(right);
                    left
                },
            },
            Input::LeftRightVec(left, right) => rosc::OscMessage {
                addr: "/tracking/eye/LeftRightVec".to_string(),
                args: {
                    let mut left: Vec<rosc::OscType> = left.into();
                    let right: Vec<rosc::OscType> = right.into();
                    left.extend(right);
                    left
                },
            },
        }
    }
}

// https://docs.vrchat.com/docs/osc-avatar-parameters
// https://creators.vrchat.com/avatars/animator-parameters
/// Messages sent from the game client.
#[derive(Debug, Clone, PartialEq)]
pub enum Output {
    Change(String),

    IsLocal(bool),
    Grounded(bool),
    Seated(bool),
    Afk(bool),
    MuteSelf(bool),
    InStation(bool),
    Earmuffs(bool),

    GestureLeftWeight(f32),
    GestureRightWeight(f32),
    AngularY(f32),
    VelocityX(f32),
    VelocityY(f32),
    VelocityZ(f32),
    VelocityMagnitude(f32),
    Upright(f32),
    Voice(f32),

    Viseme(Viseme),
    GestureLeft(Gesture),
    GestureRight(Gesture),
    TrackingType(TrackingType),
    VRMode(i32),

    ScaleModified(bool),
    ScaleFactor(f32),
    ScaleFactorInverse(f32),
    EyeHeightAsMeters(f32),
    EyeHeightAsPercent(f32),

    // NOTE: (address, value)
    Custom(String, ParameterType),
}

impl TryFrom<rosc::OscMessage> for Output {
    type Error = error::InvalidOscMessage;

    fn try_from(value: rosc::OscMessage) -> Result<Self, Self::Error> {
        let e = Err(
            Self::Error {
                location: stringify!(AvatarOutput),
                value: value.clone(),
            }
        );

        let value = match (value.addr.as_str(), value.args.as_slice()) {
            ("/avatar/change", [rosc::OscType::String(v)]) => Self::Change(v.to_string()),

            ("/avatar/parameters/IsLocal", [rosc::OscType::Bool(v)]) => Self::IsLocal(*v),
            ("/avatar/parameters/Grounded", [rosc::OscType::Bool(v)]) => Self::Grounded(*v),
            ("/avatar/parameters/Seated", [rosc::OscType::Bool(v)]) => Self::Seated(*v),
            ("/avatar/parameters/AFK", [rosc::OscType::Bool(v)]) => Self::Afk(*v),
            ("/avatar/parameters/MuteSelf", [rosc::OscType::Bool(v)]) => Self::MuteSelf(*v),
            ("/avatar/parameters/InStation", [rosc::OscType::Bool(v)]) => Self::InStation(*v),
            ("/avatar/parameters/Earmuffs", [rosc::OscType::Bool(v)]) => Self::Earmuffs(*v),

            ("/avatar/parameters/GestureLeftWeight", [rosc::OscType::Float(v)]) => Self::GestureLeftWeight(*v),
            ("/avatar/parameters/GestureRightWeight", [rosc::OscType::Float(v)]) => Self::GestureRightWeight(*v),
            ("/avatar/parameters/AngularY", [rosc::OscType::Float(v)]) => Self::AngularY(*v),
            ("/avatar/parameters/VelocityX", [rosc::OscType::Float(v)]) => Self::VelocityX(*v),
            ("/avatar/parameters/VelocityY", [rosc::OscType::Float(v)]) => Self::VelocityY(*v),
            ("/avatar/parameters/VelocityZ", [rosc::OscType::Float(v)]) => Self::VelocityZ(*v),
            ("/avatar/parameters/Upright", [rosc::OscType::Float(v)]) => Self::Upright(*v),
            ("/avatar/parameters/Voice", [rosc::OscType::Float(v)]) => Self::Voice(*v),

            ("/avatar/parameters/Viseme", [rosc::OscType::Int(v)]) => {
                if let Ok(v) = Viseme::try_from(*v) {
                    Self::Viseme(v)
                } else {
                    return e;
                }
            },
            ("/avatar/parameters/GestureLeft", [rosc::OscType::Int(v)]) => {
                if let Ok(v) = Gesture::try_from(*v) {
                    Self::GestureLeft(v)
                } else {
                    return e;
                }
            },
            ("/avatar/parameters/GestureRight", [rosc::OscType::Int(v)]) => {
                if let Ok(v) = Gesture::try_from(*v) {
                    Self::GestureRight(v)
                } else {
                    return e;
                }
            },
            ("/avatar/parameters/TrackingType", [rosc::OscType::Int(v)]) => {
                if let Ok(v) = TrackingType::try_from(*v) {
                    Self::TrackingType(v)
                } else {
                    return e;
                }
            },
            ("/avatar/parameters/VRMode", [rosc::OscType::Int(v)]) => Self::VRMode(*v),

            ("/avatar/parameters/ScaleModified", [rosc::OscType::Bool(v)]) => Self::ScaleModified(*v),
            ("/avatar/parameters/ScaleFactor", [rosc::OscType::Float(v)]) => Self::ScaleFactor(*v),
            ("/avatar/parameters/ScaleFactorInverse", [rosc::OscType::Float(v)]) => Self::ScaleFactorInverse(*v),
            ("/avatar/parameters/EyeHeightAsMeters", [rosc::OscType::Float(v)]) => Self::EyeHeightAsMeters(*v),
            ("/avatar/parameters/EyeHeightAsPercent", [rosc::OscType::Float(v)]) => Self::EyeHeightAsPercent(*v),

            (addr, [rosc::OscType::Bool(v)]) => Self::Custom(addr.to_owned(), ParameterType::Bool(*v)),
            (addr, [rosc::OscType::Int(v)]) => {
                if let Ok(v) = u8::try_from(*v) {
                    Self::Custom(addr.to_owned(), ParameterType::Int(v))
                } else {
                    return e;
                }
            },
            (addr, [rosc::OscType::Float(v)]) => Self::Custom(addr.to_owned(), ParameterType::Float(*v)),

            _ => return e
        };

        Ok(value)
    }
}

// TODO: Expression Parameter Aliasing.
// https://creators.vrchat.com/avatars/animator-parameters/

// TODO: Sync Types.
// https://creators.vrchat.com/avatars/animator-parameters/
